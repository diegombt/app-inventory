from django.apps import AppConfig


class ItPlatformConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'it_platform'
