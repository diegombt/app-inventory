from django.shortcuts import get_object_or_404, render

from .models import Application, Server, Framework, OperatingSystem, Database


def index(request):
    application_list = Application.objects.all().order_by('id')
    return render(request, "it_platform/applications.html", {
        "application_list": application_list
    })


def app_details(request, application_id):
    application = get_object_or_404(Application, pk=application_id)
    return render(request, "it_platform/app_details.html", {
        "application": application
    })


def server_details(request, server_id):
    server = get_object_or_404(Server, pk=server_id)
    return render(request, "it_platform/server_details.html", {
        "server": server
    })

def frameworks(request):
    framework_list = Framework.objects.all().order_by('name', 'version')
    return render(request, "it_platform/frameworks.html", {
        "framework_list": framework_list
    })

def operatingSystems(request):
    os_list = OperatingSystem.objects.all().order_by('family', 'version')
    return render(request, "it_platform/operating_systems.html", {
        "os_list": os_list
    })

def servers(request):
    server_list = Server.objects.all().order_by('name')
    return render(request, "it_platform/servers.html", {
        "server_list": server_list
    })

def databases(request):
    db_list = Database.objects.all().order_by('name')
    return render(request, "it_platform/databases.html", {
        "db_list": db_list
    })

def database_details(request, database_id):
    database = get_object_or_404(Database, pk=database_id)
    return render(request, "it_platform/database_details.html", {
        "database": database
    })