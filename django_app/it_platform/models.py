from email.mime import application
from django.db import models

class OperatingSystem(models.Model):
    class Meta:
        db_table = u'"it\".\"operating_systems"'

    FAMILIES = [('WI', 'Windows'), ('LI', 'Linux'), ('MA', 'macOS'), ('UN', 'Unix'), ('VM','VMware'), ('AWS','AWS'), ('AZ','Azure'),]

    family = models.CharField(
        max_length=3,
        choices=FAMILIES,
        default='WI',
    )
    version = models.CharField(max_length=30)
    edition = models.CharField(max_length=30)
    distribution = models.CharField(max_length=30, blank=True)
    compilation = models.CharField(max_length=30, blank=True)
    architecture = models.CharField(max_length=30, blank=True)
    releaseDate = models.DateField(db_column='release_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    supportEndDate = models.DateField(db_column='support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    extendedSupportEndDate = models.DateField(db_column='extended_support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return self.get_family_display() +" "+ self.version +" ("+ self.edition + ")"


class Server(models.Model):
    class Meta:
        db_table = u'"it\".\"servers"'

    name = models.CharField(max_length=30, unique=True)
    operatingSystem = models.ForeignKey(OperatingSystem, on_delete=models.CASCADE, db_column='operating_system_id')
    details = models.CharField(max_length=200, blank=True)
    host = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    location = models.CharField(max_length=200, blank=True)
    processors = models.CharField(max_length=200, blank=True)
    physicalMemoryGB = models.CharField(db_column='physical_memory_gb', max_length=200, blank=True)
    storageGB = models.CharField(db_column='storage_gb', max_length=200, blank=True)
    category = models.CharField(max_length=200, blank=True)
    state = models.CharField(max_length=200, blank=True)
    environment = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.name


class DBManagementSystem(models.Model):
    class Meta:
        db_table = u'"it\".\"db_management_systems"'

    family = models.CharField(max_length=30)
    version = models.CharField(max_length=30)
    edition = models.CharField(max_length=30)
    compilation = models.CharField(max_length=30, blank=True)
    architecture = models.CharField(max_length=30, blank=True)
    type = models.CharField(max_length=30, blank=True)
    releaseDate = models.DateField(db_column='release_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    supportEndDate = models.DateField(db_column='support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    extendedSupportEndDate = models.DateField(db_column='extended_support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return self.family +" "+ self.version +" "+ self.edition


class Database(models.Model):
    class Meta:
        db_table = u'"it\".\"databases"'

    name = models.CharField(max_length=100, null=False, unique=True)
    DBManagementSystem = models.ManyToManyField(DBManagementSystem, through='DatabaseInstance', db_column='db_management_system_id')
    highAvailabilityType = models.CharField(db_column='high_availability_type', max_length=30, blank=True)
    server = models.ManyToManyField(Server, through='DatabaseInstance')

    def __str__(self):
        return self.name

    def databaseInstances(self):
        return DatabaseInstance.objects.filter(server__in=self.server.all(), database=self)


class BusinessUnit(models.Model):
    class Meta:
        db_table = u'"it\".\"business_units"'

    name = models.CharField(max_length=35, null=False)
    details = models.CharField(max_length=200, blank=True,null=True)

    def __str__(self):
        return self.name


class Framework(models.Model):
    class Meta:
        db_table = u'"it\".\"frameworks"'

    name = models.CharField(max_length=30)
    version = models.CharField(max_length=30)
    language = models.CharField(max_length=30, blank=True)
    type = models.CharField(max_length=30, blank=True)
    license = models.CharField(max_length=30, blank=True)
    releaseDate = models.DateField(db_column='release_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    supportEndDate = models.DateField(db_column='support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    extendedSupportEndDate = models.DateField(db_column='extended_support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return self.name +" - "+ self.version


class Application(models.Model):
    class Meta:
        db_table = u'"it\".\"applications"'
        constraints = [
            models.UniqueConstraint(fields=['businessUnit', 'codeID', 'name', 'type'], name='unique_application')
        ]
    
    MISSION_CRITICAL_CATEGORIES = [
        ('HI', 'High'),
        ('ME', 'Medium'),
        ('LO', 'Low'),
    ]

    businessUnit = models.ForeignKey(BusinessUnit, on_delete=models.CASCADE, db_column='business_unit_id')
    codeID = models.CharField(max_length=30, unique=True, db_column='code_id')
    name = models.CharField(max_length=50)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    type = models.CharField(max_length=30, blank=True)
    state = models.CharField(max_length=30, blank=True)
    missionCritical = models.CharField(
        max_length=2,
        choices=MISSION_CRITICAL_CATEGORIES,
        default='LO',
        blank=True,
        db_column='mission_critical'
    )
    category = models.CharField(max_length=30, blank=True)
    highAvailabilityType = models.CharField("High availability type", db_column='high_availability_type', max_length=30, blank=True)
    releaseDate = models.DateField("Release date", db_column='release_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    supportEndDate = models.DateField("Support end date", db_column='support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    extendedSupportEndDate = models.DateField("Extended support end date", db_column='extended_support_end_date', auto_now=False, auto_now_add=False, blank=True, null=True)
    version = models.CharField(max_length=30, blank=True)
    edition = models.CharField(max_length=30, blank=True)
    compilation = models.CharField(max_length=30, blank=True)
    server = models.ManyToManyField(Server, through='ApplicationInstance')
    framework = models.ManyToManyField(Framework, through='ApplicationInstance')
    database = models.ManyToManyField(Database, through='DatabaseInstance')

    def __str__(self):
        return self.name +" - "+ self.codeID

    def appInstances(self):
        return ApplicationInstance.objects.filter(server__in=self.server.all(), application=self)


class ApplicationInstance(models.Model):
    class Meta:
        db_table = u'"it\".\"application_instance"'
        constraints = [
            models.UniqueConstraint(fields=['application', 'server', 'framework', 'appInstance'], name='unique_application_instance')
        ]
    
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    framework = models.ForeignKey(Framework , on_delete=models.CASCADE)
    appInstance = models.CharField(db_column='app_instance', max_length=50, default='DEFAULT')
    createdAt = models.DateTimeField(db_column='created_at', auto_now=False, auto_now_add=True)
    updatedAt = models.DateTimeField(db_column='updated_at', auto_now=True, auto_now_add=False)
    removedAt = models.DateTimeField(db_column='removed_at', auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return self.application.name +" - "+ self.server.name +" - "+ self.framework.version


class DatabaseInstance(models.Model):
    class Meta:
        db_table = u'"it\".\"database_instance"'
        constraints = [
            models.UniqueConstraint(fields=['database', 'server', 'application', 'dbInstance'], name='unique_database_instance')
        ]
    
    database = models.ForeignKey(Database, on_delete=models.CASCADE)
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    dbInstance = models.CharField(db_column='db_instance', max_length=50, default='DEFAULT')
    DBManagementSystem = models.ForeignKey(DBManagementSystem, on_delete=models.CASCADE, null=True, blank=True, db_column='db_management_system_id')
    state = models.CharField(max_length=30, blank=True)
    createdAt = models.DateTimeField(db_column='created_at', auto_now=False, auto_now_add=True)
    updatedAt = models.DateTimeField(db_column='updated_at', auto_now=True, auto_now_add=False)
    removedAt = models.DateTimeField(db_column='removed_at', auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return self.database.name +" <-> "+ self.server.name