from django.contrib import admin

from .models import Application, ApplicationInstance, BusinessUnit, Database, DatabaseInstance, DBManagementSystem, Framework, OperatingSystem, Server


# Inline models
class ApplicationInstanceInline(admin.TabularInline):
    model = ApplicationInstance
    extra = 0


class DatabaseInstanceInline(admin.StackedInline):
    model = DatabaseInstance
    extra = 0


# Admin models
class ApplicationAdmin(admin.ModelAdmin):
    inlines = [ApplicationInstanceInline, DatabaseInstanceInline]
    list_display = ("name", "codeID", "type", "state")
    search_fields = ["name", "category", "codeID"]


class DatabaseAdmin(admin.ModelAdmin):
    inlines = [DatabaseInstanceInline]
    list_display = ("name",)
    search_fields = ["name"]


class ServerAdmin(admin.ModelAdmin):
    inlines = [DatabaseInstanceInline, ApplicationInstanceInline]
    list_display = ("name", "operatingSystem", "environment", "state")
    search_fields = ["name"]


class OperatingSystemAdmin(admin.ModelAdmin):
    list_display = ("family", "version", "edition", "distribution")


class DBManagementSystemAdmin(admin.ModelAdmin):
    list_display = ("family", "version", "edition", "type", "supportEndDate")
    search_fields = ["family", "version", "edition"]


class FrameworkAdmin(admin.ModelAdmin):
    list_display = ("name", "version", "supportEndDate")
    search_fields = ["name", "version"]


# #############################################################################
admin.site.register(Application, ApplicationAdmin)
admin.site.register(BusinessUnit)
admin.site.register(Database, DatabaseAdmin)
admin.site.register(DBManagementSystem, DBManagementSystemAdmin)
admin.site.register(Framework, FrameworkAdmin)
admin.site.register(OperatingSystem, OperatingSystemAdmin)
admin.site.register(Server, ServerAdmin)