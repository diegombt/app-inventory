from django.urls import path

from . import views


app_name = "it_platform"
urlpatterns = [
    path('', views.index, name='index'),
    path('app_details/<int:application_id>', views.app_details, name='app_details'),
    path('server_details/<int:server_id>', views.server_details, name='server_details'),
    path('frameworks', views.frameworks, name='frameworks'),
    path('operatingSystems', views.operatingSystems, name='operatingSystems'),
    path('servers', views.servers, name='servers'),
    path('databases', views.databases, name='databases'),
    path('database_details/<int:database_id>', views.database_details, name='database_details')
]
